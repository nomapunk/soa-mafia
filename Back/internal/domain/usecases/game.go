package usecases

import (
	"fmt"
	"gitlab.com/nomapunk/soa-mafia/internal/proto"
	"log"
	"sync"
)

type Game struct {
	room    *Room
	game    *proto.Game
	wg      sync.WaitGroup
	mtx     sync.Mutex
	clients []*GameClient
}

func (g *Game) SendSelectPlayers(event *proto.Event, selector func(int) bool, wait bool) {
	cnt := 0
	for i := 0; i < len(g.clients); i++ {
		if selector(i) {
			cnt += 1
		}
	}
	if wait {
		g.mtx.Lock()
		g.wg.Add(cnt)
		g.game.Votes = make([]*proto.Vote, 0)
		g.mtx.Unlock()
	}
	for i := 0; i < len(g.clients); i++ {
		if selector(i) {
			chatEmpty := make([]string, 0)
			chatEmpty = append(chatEmpty, "Chat mode: "+g.room.chatStatus.String())
			var current_chat []string
			current_chat = make([]string, 1)
			copy(current_chat, chatEmpty)
			g.room.mtx.Lock()
			var roomchat []string
			roomchat = make([]string, len(g.room.room.Chat))
			copy(roomchat, g.room.room.Chat)
			g.room.mtx.Unlock()
			canSend := "false"
			if g.room.chatStatus == proto.Character_MAFIA {
				if g.room.room.Game.CurrentPlayers[i].Character != proto.Character_MAFIA {
				} else {
					current_chat = make([]string, len(roomchat))
					copy(current_chat, roomchat)
					canSend = "true"
				}
			} else if g.room.chatStatus == proto.Character_MEDIC {
				if g.room.room.Game.CurrentPlayers[i].Character != proto.Character_MEDIC {
				} else {
					current_chat = make([]string, len(roomchat))
					copy(current_chat, roomchat)
					canSend = "true"
				}
			} else if g.room.chatStatus == proto.Character_SHERIFF {
				if g.room.room.Game.CurrentPlayers[i].Character != proto.Character_SHERIFF {
				} else {
					current_chat = make([]string, len(roomchat))
					copy(current_chat, roomchat)
					canSend = "true"
				}
			} else {
				current_chat = make([]string, len(roomchat))
				copy(current_chat, roomchat)
				canSend = "true"
			}
			log.Println(event.Text, g.clients[i].game.room.room.Players[i].Name)
			text := event.Text
			alive := "true"
			if len(g.clients[i].game.game.CurrentPlayers) > 0 && !g.clients[i].game.game.CurrentPlayers[i].Alive {
				alive = "false"
			}
			if event.Type == proto.EventEnum_END {
				alive = "false"
			}
			currentEvent := &proto.Event{
				Text:      text,
				Type:      event.Type,
				Response:  event.Response,
				RoomId:    g.room.room.RoomId,
				UserId:    event.UserId,
				Room:      g.room.room,
				Character: g.clients[i].game.game.CurrentPlayers[i].Character.String(),
				Chat:      current_chat,
				CanSend:   canSend,
				Alive:     alive,
			}
			err := g.clients[i].srv.Send(currentEvent)
			if err != nil {
				log.Fatal(err)
				return
			}
		}
	}
	if wait {
		g.wg.Wait()
	}
}

func (g *Game) GetMaxIndex() int {
	cnt := make([]int, len(g.clients))
	maxIndex := 0
	for _, vote := range g.game.Votes {
		cnt[vote.To]++
		if cnt[maxIndex] < cnt[vote.To] {
			maxIndex = int(vote.To)
		}
	}
	return maxIndex
}

func (g *Game) Run() {
	defer func() {
		for i := 0; i < len(g.game.CurrentPlayers); i++ {
			g.room.done <- true
		}
	}()
	check := func() (bool, bool, bool, int) {
		mafiaAlive := false
		sheriffAlive := false
		medicAlive := false
		cntNotMafia := 0
		for index, _ := range g.clients {
			log.Println(index, g.game.CurrentPlayers[index].Player.Name, g.game.CurrentPlayers[index].Character.String())
			player := g.game.CurrentPlayers[index]
			if !player.Alive {
				continue
			}
			if player.Character == proto.Character_MAFIA {
				mafiaAlive = true
			} else {
				cntNotMafia++
				if player.Character == proto.Character_SHERIFF {
					sheriffAlive = true
				} else if player.Character == proto.Character_MEDIC {
					medicAlive = true
				}
			}
		}
		return mafiaAlive, sheriffAlive, medicAlive, cntNotMafia
	}
	for {
		mafiaAlive, sheriffAlive, medicAlive, cntNotMafia := check()
		log.Printf("ROOM %d, cntNotMafia = %d", g.room.room.RoomId, cntNotMafia)
		if !mafiaAlive {
			g.room.FinishGame(false)
			g.SendSelectPlayers(
				&proto.Event{
					Text: "MAFIA LOSE!",
					Type: proto.EventEnum_END,
				},
				func(i int) bool {
					return true
				},
				false,
			)
			return
		}
		if cntNotMafia <= 1 {
			g.room.FinishGame(true)
			g.SendSelectPlayers(
				&proto.Event{
					Text: "MAFIA WIN!",
					Type: proto.EventEnum_END,
				},
				func(i int) bool {
					return true
				},
				false,
			)
			return
		}
		g.SendSelectPlayers(
			&proto.Event{
				Text: "Night...",
				Type: proto.EventEnum_NIGHT_EVENT,
			},
			func(i int) bool {
				return true
			},
			false,
		)
		if sheriffAlive {
			sheriffCheck := -1
			g.room.mtx.Lock()
			g.room.room.Chat = make([]string, 0)
			g.room.room.Chat = append(g.room.room.Chat, "Chat mode: SHERIFF")
			g.room.chatStatus = proto.Character_SHERIFF
			g.room.mtx.Unlock()
			g.SendSelectPlayers(
				&proto.Event{
					Text:   "Sheriff time to chose!",
					RoomId: g.room.room.RoomId,
					Type:   proto.EventEnum_NO_ANSWER,
				},
				func(i int) bool {
					return true
				},
				false,
			)
			g.SendSelectPlayers(
				&proto.Event{
					Text:   "Sheriff, select the player you want to check",
					RoomId: g.room.room.RoomId,
					Type:   proto.EventEnum_MUST_ANSWER,
				},
				func(i int) bool {
					player := g.game.CurrentPlayers[i]
					return player.Alive && player.Character == proto.Character_SHERIFF
				},
				true,
			)
			sheriffCheck = g.GetMaxIndex()
			g.SendSelectPlayers(
				&proto.Event{
					Text: fmt.Sprintf("Sheriff, player %s is %s",
						g.game.CurrentPlayers[sheriffCheck].Player.Name,
						g.game.CurrentPlayers[sheriffCheck].Character.String(),
					),
					RoomId: g.room.room.RoomId,
					Type:   proto.EventEnum_NO_ANSWER,
				},
				func(i int) bool {
					player := g.game.CurrentPlayers[i]
					return player.Alive && player.Character == proto.Character_SHERIFF
				},
				false,
			)
		}
		protectID := -1
		if medicAlive {
			g.room.mtx.Lock()
			g.room.room.Chat = make([]string, 0)
			g.room.room.Chat = append(g.room.room.Chat, "Chat mode: MEDIC")
			g.room.chatStatus = proto.Character_MEDIC
			g.room.mtx.Unlock()
			g.SendSelectPlayers(
				&proto.Event{
					Text:   "Medic time to chose!",
					RoomId: g.room.room.RoomId,
					Type:   proto.EventEnum_NO_ANSWER,
				},
				func(i int) bool {
					return true
				},
				false,
			)
			g.SendSelectPlayers(
				&proto.Event{
					Text:   "Medic, select the player you want to protect",
					RoomId: g.room.room.RoomId,
					Type:   proto.EventEnum_MUST_ANSWER,
				},
				func(i int) bool {
					player := g.game.CurrentPlayers[i]
					return player.Alive && player.Character == proto.Character_MEDIC
				},
				true,
			)
		}
		protectID = g.GetMaxIndex()
		log.Printf("Try get protectID: %d heal %s", protectID, g.game.CurrentPlayers[protectID].Player.Name)
		g.room.mtx.Lock()
		g.room.room.Chat = make([]string, 0)
		g.room.room.Chat = append(g.room.room.Chat, "Chat mode: MAFIA")
		g.room.chatStatus = proto.Character_MAFIA
		g.room.mtx.Unlock()
		g.SendSelectPlayers(
			&proto.Event{
				Text:   "Mafia time to chose!",
				RoomId: g.room.room.RoomId,
				Type:   proto.EventEnum_NO_ANSWER,
			},
			func(i int) bool {
				return true
			},
			false,
		)
		g.SendSelectPlayers(
			&proto.Event{
				Text:   "Mafia, select the player you want to kill",
				RoomId: g.room.room.RoomId,
				Type:   proto.EventEnum_MUST_ANSWER,
			},
			func(i int) bool {
				player := g.game.CurrentPlayers[i]
				return player.Alive && player.Character == proto.Character_MAFIA
			},
			true,
		)
		maxIndex := g.GetMaxIndex()
		log.Printf("Try get killedID: %d kill %s", maxIndex, g.game.CurrentPlayers[maxIndex].Player.Name)
		g.SendSelectPlayers(
			&proto.Event{
				Text: "Day...",
				Type: proto.EventEnum_NIGHT_EVENT,
			},
			func(i int) bool {
				return true
			},
			false,
		)
		if maxIndex == protectID {
			g.SendSelectPlayers(
				&proto.Event{
					Text: "All people alive, medic nice work!",
				},
				func(i int) bool {
					return true
				},
				false,
			)
		} else {
			g.mtx.Lock()
			g.game.CurrentPlayers[maxIndex].Alive = false
			g.mtx.Unlock()
			g.SendSelectPlayers(
				&proto.Event{
					Text: fmt.Sprintf("User %s has been killed and his role: %s",
						g.game.CurrentPlayers[maxIndex].Player.Name,
						g.game.CurrentPlayers[maxIndex].Character.String(),
					),
				},
				func(i int) bool {
					return true
				},
				false,
			)
		}
		mafiaAlive, sheriffAlive, medicAlive, cntNotMafia = check()
		log.Printf("ROOM %d, cntNotMafia = %d", g.room.room.RoomId, cntNotMafia)
		if !mafiaAlive {
			g.room.FinishGame(false)
			g.SendSelectPlayers(
				&proto.Event{
					Text: "MAFIA LOSE!",
					Type: proto.EventEnum_END,
				},
				func(i int) bool {
					return true
				},
				false,
			)
			return
		}
		if cntNotMafia <= 1 {
			g.room.FinishGame(true)
			g.SendSelectPlayers(
				&proto.Event{
					Text: "MAFIA WIN!",
					Type: proto.EventEnum_END,
				},
				func(i int) bool {
					return true
				},
				false,
			)
			return
		}
		g.room.mtx.Lock()
		g.room.room.Chat = make([]string, 0)
		g.room.room.Chat = append(g.room.room.Chat, "Chat mode: CITIZEN")
		g.room.chatStatus = proto.Character_CITIZEN
		g.room.mtx.Unlock()
		g.SendSelectPlayers(
			&proto.Event{
				Text:   "Vote!!!!",
				RoomId: g.room.room.RoomId,
				Type:   proto.EventEnum_NO_ANSWER,
			},
			func(i int) bool {
				return true
			},
			false,
		)
		g.SendSelectPlayers(
			&proto.Event{
				Text: "Vote! Select the player you want to vote",
				Type: proto.EventEnum_MUST_ANSWER,
			},
			func(i int) bool {
				return g.game.CurrentPlayers[i].Alive
			},
			true,
		)
		maxIndex = g.GetMaxIndex()
		g.SendSelectPlayers(
			&proto.Event{
				Text: fmt.Sprintf(
					"User %s has been kicked and his role: %s",
					g.game.CurrentPlayers[maxIndex].Player.Name,
					g.game.CurrentPlayers[maxIndex].Character.String()),
				Type: proto.EventEnum_NO_ANSWER,
			},
			func(i int) bool {
				return true
			},
			false,
		)
		g.mtx.Lock()
		g.game.CurrentPlayers[maxIndex].Alive = false
		g.mtx.Unlock()
	}
}

type GameClient struct {
	game *Game
	srv  proto.MafiaService_JoinServer
}
