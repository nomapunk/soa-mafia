package usecases

import (
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/nomapunk/soa-mafia/internal/proto"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"sync"
	"time"
)

type Room struct {
	room       *proto.Room
	g          *Game
	mtx        sync.Mutex
	wg         sync.WaitGroup
	done       chan bool
	chatStatus proto.Character
	startTime  time.Time
}

func (r *Room) FinishGame(mafiaWin bool) {
	finishTime := time.Now()
	for _, val := range r.room.Game.CurrentPlayers {
		user := val.Player.Name
		delta_win := 0
		delta_games := 1
		delta_time := finishTime.Sub(r.startTime).Seconds()
		if mafiaWin && val.Character == proto.Character_MAFIA {
			delta_win = 1
		} else if !mafiaWin && val.Character != proto.Character_MAFIA {
			delta_win = 1
		}
		baseURL := "http://stats:8083"
		resource := "/newResult"
		params := url.Values{}
		params.Add("delta_win", fmt.Sprintf("%d", delta_win))
		params.Add("delta_games", fmt.Sprintf("%d", delta_games))
		params.Add("delta_time", fmt.Sprintf("%d", int(delta_time)))
		params.Add("user", user)
		log.Println(delta_win, delta_games, delta_time, user)
		u, _ := url.ParseRequestURI(baseURL)
		u.Path = resource
		u.RawQuery = params.Encode()
		urlStr := fmt.Sprintf("%v", u)
		_, err := http.Get(urlStr)
		if err != nil {
			log.Println(err)
		}
	}
}

func (r *Room) RunChat() {
	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		log.Println("Failed Initializing Broker Connection")
	}
	ch, err := conn.Channel()
	if err != nil {
		log.Println(err)
	}
	defer ch.Close()
	q, err := ch.QueueDeclare(
		fmt.Sprintf("room%d", r.room.RoomId),
		false,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		fmt.Println(err)
	}

	messages, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	for message := range messages {
		r.mtx.Lock()
		r.room.Chat = append(r.room.Chat, string(message.Body))
		if len(r.room.Chat) > 10 {
			r.room.Chat = r.room.Chat[1:]
		}
		r.mtx.Unlock()
		for index, value := range r.g.clients {
			r.mtx.Lock()
			chatEmpty := make([]string, 0)
			chatEmpty = append(chatEmpty, "Chat mode: "+r.chatStatus.String())
			var roomchat []string
			roomchat = make([]string, len(r.room.Chat))
			copy(roomchat, r.room.Chat)
			r.mtx.Unlock()
			log.Println(string(message.Body))
			current_chat := make([]string, len(chatEmpty))
			copy(current_chat, chatEmpty)
			canSend := "false"
			if r.chatStatus == proto.Character_MAFIA {
				if value.game.game.CurrentPlayers[index].Character != proto.Character_MAFIA {
				} else {
					current_chat = make([]string, len(roomchat))
					copy(current_chat, roomchat)
					canSend = "true"
				}
			} else if r.chatStatus == proto.Character_MEDIC {
				if value.game.game.CurrentPlayers[index].Character != proto.Character_MEDIC {
				} else {
					current_chat = make([]string, len(roomchat))
					canSend = "true"
					copy(current_chat, roomchat)
				}
			} else if r.chatStatus == proto.Character_SHERIFF {
				if value.game.game.CurrentPlayers[index].Character != proto.Character_SHERIFF {
				} else {
					current_chat = make([]string, len(roomchat))
					canSend = "true"
					copy(current_chat, roomchat)
				}
			} else {
				current_chat = make([]string, len(roomchat))
				canSend = "true"
				copy(current_chat, roomchat)
			}
			character := "Player"
			if len(value.game.game.CurrentPlayers) > 0 {
				character = value.game.game.CurrentPlayers[index].Character.String()
			}
			alive := "true"
			if len(value.game.game.CurrentPlayers) > 0 && !value.game.game.CurrentPlayers[index].Alive {
				alive = "false"
			}
			value.srv.Send(&proto.Event{
				Type:      proto.EventEnum_NEW_MESSAGE,
				Room:      r.room,
				RoomId:    r.room.RoomId,
				Character: character,
				Chat:      current_chat,
				CanSend:   canSend,
				Alive:     alive,
			})
		}
	}
}

func (r *Room) GenerateIntoPlayers(players []*proto.Player) []*proto.IntoPlayer {
	result := make([]*proto.IntoPlayer, len(players))
	characters := make([]proto.Character, 0)
	// всегда на 4 игроков одна мафия.
	for i := 1; i*4 <= len(players); i++ {
		characters = append(characters, proto.Character_MAFIA)
	}
	characters = append(characters, proto.Character_MEDIC)
	characters = append(characters, proto.Character_SHERIFF)
	for i := 3; i < len(players); i++ {
		characters = append(characters, proto.Character_CITIZEN)
	}
	rand.Shuffle(len(characters), func(i, j int) {
		characters[i], characters[j] = characters[j], characters[i]
	})
	for i := 0; i < len(players); i++ {
		result[i] = &proto.IntoPlayer{
			Player:    players[i],
			Character: characters[i],
			Alive:     true,
		}
	}
	return result
}

func (r *Room) Join(player *proto.Player, srv proto.MafiaService_JoinServer) error {
	err := func() error {
		r.mtx.Lock()
		defer r.mtx.Unlock()
		if int32(len(r.room.Players)) == r.room.MaxPlayers {
			return fmt.Errorf("room %d overflow", r.room.RoomId)
		}
		if int32(len(r.room.Players)) == 0 {
			r.createGame()
			r.wg.Add(int(r.room.MaxPlayers))
			r.room.Players = make([]*proto.Player, 0)
		}
		r.room.Players = append(r.room.Players, player)
		log.Printf("New player in room %d (%d/%d), hello %s",
			r.room.RoomId,
			len(r.room.Players),
			r.room.MaxPlayers,
			player.Name)
		r.g.mtx.Lock()
		r.g.clients = append(r.g.clients, &GameClient{
			game: r.g,
			srv:  srv,
		})
		current_chat := make([]string, len(r.room.Chat))
		copy(current_chat, r.room.Chat)
		for _, value := range r.g.clients {
			value.srv.Send(&proto.Event{
				Text:    fmt.Sprintf("New player in room: %s", player.Name),
				Type:    proto.EventEnum_NO_ANSWER,
				Room:    r.room,
				Chat:    current_chat,
				CanSend: "true",
				Alive:   "true",
			})
		}
		r.g.mtx.Unlock()
		if int32(len(r.room.Players)) == r.room.MaxPlayers {
			r.room.Game.CurrentPlayers = r.GenerateIntoPlayers(r.room.Players)
			go func() {
				r.g.Run()
			}()
		}
		return nil
	}()
	if err != nil {
		return err
	}
	r.wg.Done()
	log.Printf("Player %s in room %d wait start game...", player.Name, r.room.RoomId)
	r.wg.Wait()
	return nil
}

func (r *Room) AddVote(vote *proto.Vote) {
	r.g.mtx.Lock()
	log.Println(vote)
	r.room.Game.Votes = append(r.room.Game.Votes, vote)
	r.g.wg.Done()
	r.g.mtx.Unlock()
}

func (r *Room) createGame() {
	r.g = &Game{
		room:    r,
		game:    r.room.Game,
		wg:      sync.WaitGroup{},
		mtx:     sync.Mutex{},
		clients: make([]*GameClient, 0),
	}
}
