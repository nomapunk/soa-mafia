package grpc_server

import (
	"context"
	"gitlab.com/nomapunk/soa-mafia/internal/domain/usecases"
	"gitlab.com/nomapunk/soa-mafia/internal/proto"
	"google.golang.org/protobuf/types/known/emptypb"
)

type GRPCServer struct {
	proto.UnimplementedMafiaServiceServer
	server *usecases.Server
}

func (s *GRPCServer) Join(in *proto.Player, srv proto.MafiaService_JoinServer) error {
	return s.server.Join(in, srv)
}

func (s *GRPCServer) ResponseEvent(ctx context.Context, event *proto.Event) (*emptypb.Empty, error) {
	s.server.ResponseEvent(event)
	return &emptypb.Empty{}, nil
}

func (s *GRPCServer) GetPlayersList(ctx context.Context, room *proto.Room) (*proto.Players, error) {
	return s.server.GetPlayersList(room)
}

func NewServer() *GRPCServer {
	return &GRPCServer{
		server: usecases.NewServer(),
	}
}
