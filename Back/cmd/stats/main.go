package main

import (
	"fmt"
	"github.com/signintech/gopdf"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
)

type Store struct {
	avatar    map[string]string
	cnt_games map[string]int
	cnt_win   map[string]int
	pdf_store map[string][]byte
	sum_time  map[string]time.Duration
	mtx       sync.Mutex
}

var store = &Store{
	avatar:    make(map[string]string, 0),
	cnt_games: make(map[string]int, 0),
	cnt_win:   make(map[string]int, 0),
	sum_time:  make(map[string]time.Duration, 0),
	pdf_store: make(map[string][]byte, 0),
	mtx:       sync.Mutex{},
}

func create_pdf(user string, url string, cnt_win int, cnt_games int, sum_time time.Duration, avatar string, id string) {
	log.Println("TRY CREATE PDF", user, url, cnt_win, cnt_games, sum_time, avatar)
	pdf := gopdf.GoPdf{}
	pdf.Start(gopdf.Config{PageSize: *gopdf.PageSizeA4}) //595.28, 841.89 = A4
	err := pdf.AddTTFFont("LiberationSerif-Regular", "./shrifts/LiberationSerif-Regular.ttf")
	if err != nil {
		log.Print(err.Error())
		return
	}
	err = pdf.SetFont("LiberationSerif-Regular", "", 14)
	if err != nil {
		log.Print(err.Error())
		return
	}
	pdf.AddPage()

	pdf.SetXY(30, 40)
	if err := pdf.Text(user + "'s stat"); err != nil {
		log.Println(err)
	}
	pdf.AddPage()

	pdf.SetXY(30, 40)
	if err := pdf.Image("images/"+avatar, 200, 50, nil); err != nil {
		log.Println(err)
	}
	pdf.AddPage()

	pdf.SetXY(30, 40)
	if err := pdf.Text(fmt.Sprintf("cnt_win = %d", cnt_win)); err != nil {
		log.Println(err)
	}
	pdf.AddPage()

	pdf.SetXY(30, 40)
	if err := pdf.Text(fmt.Sprintf("cnt_games = %d", cnt_games)); err != nil {
		log.Println(err)
	}
	pdf.AddPage()

	pdf.SetXY(30, 40)
	if err := pdf.Text(fmt.Sprintf("sum time = %d seconds", int(sum_time.Seconds()))); err != nil {
		log.Println(err)
	}

	if err := pdf.WritePdf(id); err != nil {
		log.Println(err)
	}
	bytes, err := os.ReadFile(id)
	store.mtx.Lock()
	if _, ok := store.pdf_store[url]; !ok {
		store.pdf_store[url] = bytes
	}
	store.pdf_store[url] = bytes
	log.Println(url, len(bytes))
	store.mtx.Unlock()
}

func getStatUrl(w http.ResponseWriter, req *http.Request) {
	user := req.URL.Query().Get("user")
	id := fmt.Sprintf("%d", rand.Int31())
	url := "http://localhost:8083/getStat?id=" + id
	w.Write([]byte(url))
	go func(url_ string, user_ string) {
		log.Println("NOW ", url_, user_)
		store.mtx.Lock()
		if _, ok := store.cnt_games[user_]; !ok {
			store.cnt_games[user_] = 0
		}
		if _, ok := store.cnt_win[user_]; !ok {
			store.cnt_win[user_] = 0
		}
		if _, ok := store.sum_time[user_]; !ok {
			store.sum_time[user_] = 0
		}
		if _, ok := store.avatar[user_]; !ok {
			store.avatar[user_] = "1.png"
		}
		cnt_win := store.cnt_win[user_]
		cnt_games := store.cnt_win[user_]
		sum_time := store.sum_time[user_]
		avatar := store.avatar[user_]
		store.mtx.Unlock()
		create_pdf(user_, url_, int(cnt_win), int(cnt_games), sum_time, avatar, id)
	}(url, user)
}

func getStat(w http.ResponseWriter, req *http.Request) {
	url := req.URL.String()
	value, ok := store.pdf_store["http://localhost:8083"+url]
	log.Println(url)
	if !ok {
		w.Write([]byte("Waiting..."))
		return
	}
	w.Header().Add("Content-Type", "application/pdf")
	w.Write(value)
}

func getInt(x string) int {
	val, _ := strconv.Atoi(x)
	log.Println(x, val)
	return val
}

func newResult(w http.ResponseWriter, req *http.Request) {
	user := req.URL.Query().Get("user")
	delta_games := getInt(req.URL.Query().Get("delta_games"))
	delta_win := getInt(req.URL.Query().Get("delta_win"))
	delta_time := getInt(req.URL.Query().Get("delta_time"))
	store.mtx.Lock()
	log.Println(user, delta_games, delta_win, delta_time)
	if _, ok := store.cnt_games[user]; !ok {
		store.cnt_games[user] = 0
	}
	store.cnt_games[user] += delta_games
	if _, ok := store.cnt_win[user]; !ok {
		store.cnt_win[user] = 0
	}
	store.cnt_win[user] += delta_win
	if _, ok := store.sum_time[user]; !ok {
		store.sum_time[user] = 0
	}
	store.sum_time[user] += time.Duration(1e9 * delta_time)
	if _, ok := store.avatar[user]; !ok {
		store.avatar[user] = "1.png"
	}
	store.mtx.Unlock()
}

func changeAvatar(w http.ResponseWriter, req *http.Request) {
	avatar := req.URL.Query().Get("avatar")
	user := req.URL.Query().Get("user")
	store.mtx.Lock()
	store.avatar[user] = avatar
	store.mtx.Unlock()
}

func setupRoutes() {
	http.HandleFunc("/getStatUrl", getStatUrl)
	http.HandleFunc("/getStat", getStat)
	http.HandleFunc("/newResult", newResult)
	http.HandleFunc("/changeAvatar", changeAvatar)
}

func sendMessageEndPoint(w http.ResponseWriter, req *http.Request) {
}

func main() {
	setupRoutes()
	log.Println("Start stats...")
	http.ListenAndServe(":8083", nil)
}
