package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/streadway/amqp"
	"gitlab.com/nomapunk/soa-mafia/internal/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"
)

type AlivePlayer struct {
	Name  string `json:"name"`
	Index int32  `json:"index"`
}

type State struct {
	Users      []string      `json:"users"`
	AliveUsers []AlivePlayer `json:"aliveUsers"`
	Chat       []string      `json:"chat"`
	UserChat   []string      `json:"userChat"`
	Role       string        `json:"role"`
	RoomId     string        `json:"roomID"`
	CanSend    string        `json:"canSend"`
	Alive      string        `json:"alive"`
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func read(conn *websocket.Conn) *string {
	mt, p, err := conn.ReadMessage()
	if err != nil {
		log.Fatal(err)
		return nil
	}
	fmt.Println(string(p), mt, " <---- NEW MESSAGE")
	res := string(p)
	return &res
}

func reader(conn *websocket.Conn) {
	for {
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}
		fmt.Println(string(p))
		if err := conn.WriteMessage(messageType, p); err != nil {
			log.Println(err)
			return
		}
	}
}

func wsEndpoint(w http.ResponseWriter, r *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
	}
	role := read(ws)
	user := read(ws)
	max_players := read(ws)
	create_room := read(ws)
	mp, _ := strconv.Atoi(*max_players)
	cr, _ := strconv.Atoi(*create_room)
	log.Println("Client Connected", user, role)
	handlerJoin(ws, *user, *role, mp, cr)
}

func setupRoutes() {
	http.HandleFunc("/ws", wsEndpoint)
	http.HandleFunc("/sendMessage", sendMessageEndPoint)
}

func sendMessageEndPoint(w http.ResponseWriter, req *http.Request) {
	room_id := req.URL.Query().Get("room_id")
	user := req.URL.Query().Get("user")
	text := user + ": " + req.URL.Query().Get("text")
	log.Println(room_id, user, text)
	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		log.Println("Failed Initializing Broker Connection")
	}
	ch, err := conn.Channel()
	if err != nil {
		log.Println(err)
	}
	defer ch.Close()
	err = ch.Publish(
		"",
		fmt.Sprintf("room%s", room_id),
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(text),
		},
	)
	if err != nil {
		log.Println(err)
	}
}

func handlerJoin(ws *websocket.Conn, user string, role string, mp int, cr int) {
	if role == "bot" {
		user += fmt.Sprintf("%d", rand.Int31())
	}
	addr := ":8080"
	if len(os.Getenv("HOSTNAME")) > 0 {
		addr = "server" + addr
	}
	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal(err)
	}
	client := proto.NewMafiaServiceClient(conn)
	stream, err := client.Join(context.Background(), &proto.Player{
		Name:       user,
		CreateRoom: bool(cr == 1),
		MaxPlayers: int32(mp)})
	if err != nil {
		log.Fatal(err)
	}
	state := &State{
		Users: make([]string, 0),
		Chat:  make([]string, 0),
	}
	done := make(chan bool)
	go func() {
		for {
			resp, err := stream.Recv()
			if err == io.EOF {
				done <- true
				log.Println(err)
				break
			}
			if err != nil {
				log.Println(err)
			}
			state.UserChat = resp.Chat
			state.CanSend = resp.CanSend
			state.Alive = resp.Alive
			if resp.Type != proto.EventEnum_NEW_MESSAGE {
				state.Chat = append(state.Chat, resp.Text)
				if len(state.Chat) > 10 {
					state.Chat = state.Chat[1:]
				}
			}
			state.Users = make([]string, 0)
			state.RoomId = fmt.Sprintf("%d", resp.Room.RoomId)
			if resp.Type == proto.EventEnum_END {
				for _, val := range resp.Room.Game.CurrentPlayers {
					state.Users = append(state.Users, val.Player.Name+" is "+val.Character.String())
				}
			} else {
				for _, val := range resp.Room.Players {
					state.Users = append(state.Users, val.Name)
				}
			}
			state.AliveUsers = make([]AlivePlayer, 0)
			if resp.Room.Game != nil {
				for index, val := range resp.Room.Game.CurrentPlayers {
					if val.Alive {
						state.AliveUsers = append(state.AliveUsers, AlivePlayer{
							Name:  val.Player.Name,
							Index: int32(index),
						})
					}
				}
			}
			state.Role = resp.Character
			fmt.Printf("%s\n", resp.Text)
			bytes, _ := json.Marshal(state)
			if role != "bot" {
				if err := ws.WriteMessage(1, bytes); err != nil {
					log.Println(err)
					return
				}
			}
			go func() {
				if resp.Type == proto.EventEnum_MUST_ANSWER {
					var input string
					if role != "bot" {
						input = *read(ws)
						vali, _ := strconv.Atoi(input)
						state.Chat = append(state.Chat, fmt.Sprintf("Good choose! %s select...",
							resp.Room.Game.CurrentPlayers[vali].Player.Name))
						if len(state.Chat) > 10 {
							state.Chat = state.Chat[1:]
						}
						bytes, _ := json.Marshal(state)
						if err := ws.WriteMessage(1, bytes); err != nil {
							log.Println(err)
							return
						}
					} else {
						alive := 0
						for _, value := range resp.Room.Game.CurrentPlayers {
							if value.Alive {
								alive++
							}
						}
						i := rand.Int31() % int32(alive)
						j := 0
						for index, value := range resp.Room.Game.CurrentPlayers {
							if value.Alive {
								if int(i) == j {
									i = int32(index)
									log.Println("VOTE FROM ", user, "(", state.Role, ")", " TO ", value.Player.Name, value.Character.String())
									break
								} else {
									j++
								}
							}
						}
						input = fmt.Sprintf("%d", i)
					}
					val, err := strconv.Atoi(input)
					if err != nil || val < 0 {
						log.Println(err)
					}
					resp.Response = new(proto.Vote)
					resp.Response.To = int32(val)
					client.ResponseEvent(context.Background(), resp)
				}
			}()
		}
	}()
	<-done
}

func main() {
	rand.Seed(time.Now().UnixNano())
	setupRoutes()
	log.Println("Start client...")
	http.ListenAndServe(":8081", nil)
}
