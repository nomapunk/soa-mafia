import React, { Component } from 'react';
import { Button, Container, FormControl, Nav, Navbar, Form } from 'react-bootstrap';


function Header({login}) {
    return (
        <>
        <Navbar collapseOnSelect bg='dark' variant='dark' expand='md'>
            <Container>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Navbar.Text>
                        {"Hello, your login: " + login}
                    </Navbar.Text>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        </>
    )
}

export default Header;