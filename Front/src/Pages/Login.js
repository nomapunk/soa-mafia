import React, { useState } from 'react'
import { useNavigate } from 'react-router'
import {
  MDBContainer,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsContent,
  MDBTabsPane,
  MDBBtn,
  MDBIcon,
  MDBInput,
  MDBCheckbox
}
from 'mdb-react-ui-kit'
import axios from 'axios'

function Login({setlogin}) {
    async function login() {
        let user = document.getElementById('login-form-user').value;
        setlogin(user)
    }
    return (
      <MDBContainer className="p-3 my-5 d-flex flex-column w-50">
        <MDBTabsContent>
            <MDBInput label='Login' id='login-form-user' type='text' />
            <MDBBtn className="mb-4 w-100" onClick={login} type="button">Login</MDBBtn>
        </MDBTabsContent>
      </MDBContainer>
    );
}

export default Login;