import {Alert, Container, Row, Col} from 'react-bootstrap';
import Login from "./Login";
import React, {useState} from "react";
import {Card, Button, Form} from 'react-bootstrap'
import Header from "../Components/Header";
import {MDBBtn, MDBContainer, MDBInput, MDBTabsContent} from "mdb-react-ui-kit";
import useWebSocket from 'react-use-websocket';
import {get} from "axios";

function Message({text}) {
    return (
        <Card>
            <Card.Body>
                <Card.Text>
                    {text}
                </Card.Text>
            </Card.Body>
        </Card>
    )
}

function Hub({login, setIsGame, initArgs, setInitArgs}) {
    function CreateRoom() {
        let max_players = document.getElementById('CreateRoom').value;
        setInitArgs(["player", login, max_players, "1"])
        console.log(initArgs)
        setIsGame(true)
    }
    function JoinRoom() {
        setInitArgs(["player", login, "0", "0"])
        console.log(initArgs)
        setIsGame(true)
    }
    return (
        <MDBContainer className="p-3 my-5 d-flex flex-column w-50">
            <MDBTabsContent>
                <MDBInput label='Max players' id='CreateRoom' type='text' />
                <MDBBtn className="mb-4 w-100" onClick={CreateRoom} type="button">Create Room</MDBBtn>
                <MDBBtn className="mb-4 w-100" onClick={JoinRoom} type="button">Join room</MDBBtn>
            </MDBTabsContent>
        </MDBContainer>
    )
}

function Chat({state}) {
    let messages = state != "" ? JSON.parse(state)['chat'] : []
    console.log(messages)
    return (
        <Card>
            <Card.Header>
                Game events:
            </Card.Header>
            <Card.Body>
                {
                    messages.map(item => {
                        return (
                            <Message text={item}/>
                        )
                    })
                }
            </Card.Body>
        </Card>
    )
}

function OnlineChat({state, login}) {
    async function send() {
        let text = document.getElementById('message-send').value;
        document.getElementById('message-send').value = "";
        const options = {
            method: 'GET',
            mode: 'no-cors'
        };
        await fetch('http://localhost:8081/sendMessage?' + new URLSearchParams({
            text: text,
            user: login,
            room_id: JSON.parse(state)["roomID"]
        }), options)
    }
    let messages = state != "" ? JSON.parse(state)['userChat'] : []
    let canSend = state != "" ? JSON.parse(state)['canSend'] : "false"
    let alive = state != "" ? JSON.parse(state)['alive'] : "false"
    return (
        <Card>
            <Card.Header>
                Online chat for room#{state != "" ? JSON.parse(state)["roomID"] : "none"}:
            </Card.Header>
            <Card.Body>
                {
                    messages.map(item => {
                        return (
                            <Message text={item}/>
                        )
                    })
                }
                {
                    canSend == "true" && alive == "true" ?
                    <Container>
                        <MDBInput id='message-send' type='text' />
                        <Button className="mb-4 w-100" onClick={send} type="button">Send</Button>
                    </Container> : <div></div>
                }
            </Card.Body>
        </Card>
    )
}

function PlayerListOne({name}) {
    return (
        <Card className='d-flex'>
            <Card.Body>
                <Card.Text>
                    Player: {name}
                </Card.Text>
            </Card.Body>
        </Card>
    )
}

function PlayerList({state}) {
    async function AddBot() {
        let ws = new WebSocket("ws://localhost:8081/ws")
        ws.onopen = function (evt) {
            console.log("OPEN WEBSOCKET")
            ws.send("bot")
            ws.send("bot")
            ws.send("0")
            ws.send("0")
        }
    }
    let players = state != "" ? JSON.parse(state)['users'] : []
    let canAddBot = state != "" ? (JSON.parse(state)['aliveUsers'].length > 0 ? false : true) : false
    console.log(players)
    return (
        <Card>
            <Card.Header>
                Players list:
            </Card.Header>
            <Card.Body>
                {
                    players.map(item => {
                        return (
                            <PlayerListOne name={item}/>
                        )
                    })
                }
                {
                    canAddBot?
                    <Button onClick={AddBot}>
                        Add bot
                    </Button> : <div></div>
                }
            </Card.Body>
        </Card>
    )
}

function AlivePlayersOne({state, name, index, getWebSocket}) {
    function Select() {
        console.log("SELECT ", index)
        getWebSocket().send(index)
    }
    let canSend = state != "" ? JSON.parse(state)['canSend'] : "false"
    let alive = state != "" ? JSON.parse(state)['alive'] : "false"
    return (
        <Card className='d-flex'>
            <Card.Body>
                <Card.Text className>
                    Player: {name}
                </Card.Text>
                {
                    canSend == "true" && alive == "true"?
                    <Button variant="primary" onClick={Select}>
                        Select
                    </Button> : <div></div>
                }
            </Card.Body>
        </Card>
    )
}

function AlivePlayerList({state, getWebSocket}) {
    let players = state != "" ? JSON.parse(state)['aliveUsers'] : []
    return (
        <Card>
            <Card.Header>
                Alive players:
            </Card.Header>
            <Card.Body>
                {
                    players.map(item => {
                        return (
                            <AlivePlayersOne name={item['name']} index={item['index']} state={state} getWebSocket={getWebSocket}/>
                        )
                    })
                }
            </Card.Body>
        </Card>
    )
}

function Game({login, setIsGame, initArgs, setInitArgs}) {
    const { sendMessage, lastMessage, readyState, getWebSocket } = useWebSocket(
        'ws://localhost:8081/ws',
        {
            onOpen: function (evt) {
                console.log("OPEN")
                console.log(initArgs)
                getWebSocket().send(initArgs[0])
                getWebSocket().send(initArgs[1])
                getWebSocket().send(initArgs[2])
                getWebSocket().send(initArgs[3])
            },
            onMessage: function (evt) {
                console.log(evt.data)
                setState(evt.data)
            }
        }
    );
    const [state, setState] = useState("")
    return (
        <div>
            <Container>
                <Row>
                    {
                        state != "" && JSON.parse(state)["role"] != "" ?
                            <div>
                                Your role: {JSON.parse(state)["role"]}
                            </div>
                            :
                            <div>
                            </div>
                    }
                </Row>
                <Row>
                    <Col>
                        <OnlineChat state={state} login={login}/>
                    </Col>
                    <Col>
                        <Chat state={state}/>
                    </Col>
                    <Col>
                        <Container>
                            <Row>
                                <PlayerList state={state}/>
                            </Row>
                            <Row>
                                <AlivePlayerList state={state} getWebSocket={getWebSocket}/>
                            </Row>
                        </Container>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

function Main({login, setlogin}) {
    const [isGame, setIsGame] = useState(false)
    const [state, SetState] = useState("")
    const [initArgs, setInitArgs] = useState("")
    return (
        login == 'NONE'
        ?
        <Login setlogin={setlogin}/>
        :
        <div>
            <Header login={login}/>
            {
                isGame ?
                    <Game login={login} setIsGame={setIsGame} initArgs={initArgs} setInitArgs = {setInitArgs}/> :
                    <Hub login={login} setIsGame={setIsGame} initArgs={initArgs} setInitArgs = {setInitArgs}/>
            }
        </div>
  );
}

export default Main;