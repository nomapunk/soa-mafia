import './App.css'
import { React } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import {useState} from 'react'
import Main from './Pages/Main'
import { BrowserRouter as Router, Routes, Route, renderMatches, BrowserRouter, useParams } from 'react-router-dom'
import axios from 'axios'

export function App() {
  const [login, setlogin] = useState('NONE');
  return (
    <div>
      <Routes>
          <Route path="/" element={<Main login={login} setlogin={setlogin}/>}/>
      </Routes>
    </div>
  );
}